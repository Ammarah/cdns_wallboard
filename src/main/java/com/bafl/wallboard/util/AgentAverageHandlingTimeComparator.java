package com.bafl.wallboard.util;

import java.util.Comparator;

import com.bafl.wallboard.model.Agent;

public class AgentAverageHandlingTimeComparator implements Comparator<Agent> {
	@Override
	public int compare(Agent agent1, Agent agent2) {
		return agent1.getAverageHandlingTimeSec() < agent2.getAverageHandlingTimeSec() ? -1
				: agent1.getAverageHandlingTimeSec() == agent2.getAverageHandlingTimeSec() ? 0 : 1;

	}
}