package com.bafl.wallboard.model;

public class Agent {	
	String agentId;
	String agentName;
	String skill;
	int totalCalls;
	String serviceTime;
	String averageHandlingTime;
	double serviceTimeInSeconds;
	double averageHandlingTimeSec;
	

	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getSkill() {
		return skill;
	}
	public void setSkill(String skill) {
		this.skill = skill;
	}
	public int getTotalCalls() {
		return totalCalls;
	}
	public void setTotalCalls(int totalCalls) {
		this.totalCalls = totalCalls;
	}
	public String getServiceTime() {
		return serviceTime;
	}
	public void setServiceTime(String serviceTime) {
		this.serviceTime = serviceTime;
	}
	public String getAverageHandlingTime() {
		return averageHandlingTime;
	}
	public void setAverageHandlingTime(String averageHandlingTime) {
		this.averageHandlingTime = averageHandlingTime;
	}
	public double getServiceTimeInSeconds() {
		return serviceTimeInSeconds;
	}
	public void setServiceTimeInSeconds(double serviceTimeInSeconds) {
		this.serviceTimeInSeconds = serviceTimeInSeconds;
	}
	public double getAverageHandlingTimeSec() {
		return averageHandlingTimeSec;
	}
	public void setAverageHandlingTimeSec(double averageHandlingTimeSec) {
		this.averageHandlingTimeSec = averageHandlingTimeSec;
	}
}
