window.addEventListener('load', function(){


    $.ajax({
        url: 'http://api.openweathermap.org/data/2.5/weather?id=1176615&units=metric&appid=b8bddf5feff7f546ee313dd9f0e46400', // returns "[1,2,3,4,5,6]"
        dataType: 'json', // jQuery will parse the response as JSON
        success: function (result) {

            if(result.clouds.all < 5)
            {   
                $('#tlb_sky').html('<br>Sunny');
                $('#tlb_img').attr("src", "images/sunny.svg");
                $('#tlp_sky').html('<br>Sunny');
                $('#tlp_img').attr("src", "images/sunny.svg");
            }
            else if(result.clouds.all > 5 && result.clouds.all < 26)
            {
                $('#tlb_sky').html('Mostly<br>Sunny');
                $('#tlb_img').attr("src", "images/mostly-sunny.svg");
                $('#tlp_sky').html('Mostly<br>Sunny');
                $('#tlp_img').attr("src", "images/mostly-sunny.svg");
            }
            else if(result.clouds.all > 25 && result.clouds.all < 46)
            {
                $('#tlb_sky').html('Partially<br>Cloudy');
                $('#tlb_img').attr("src", "images/part-cloudy.svg");
                $('#tlp_sky').html('Partially<br>Cloudy');
                $('#tlp_img').attr("src", "images/part-cloudy.svg");
            }
            else if(result.clouds.all > 45 && result.clouds.all < 70)
            {
                $('#tlb_sky').html('Mostly<br>Cloudy');
                $('#tlb_img').attr("src", "images/mostly-cloudy.svg");
                $('#tlp_sky').html('Mostly<br>Cloudy');
                $('#tlp_img').attr("src", "images/mostly-cloudy.svg");
            }
            else{
                $('#tlb_sky').html('<br>Cloudy');
                $('#tlb_img').attr("src", "images/cloudy.svg");
                $('#tlp_sky').html('<br>Cloudy');
                $('#tlp_img').attr("src", "images/cloudy.svg");
            }          
            $('#tkb_temp').html(Math.floor(result.main.temp)+'&#8451;');
            $('#tkb_hi').html(Math.floor(result.main.temp_max)+'&#8451;');
            $('#tkb_low').html(Math.floor(result.main.temp_min)+'&#8451;');
            
        }
    });
});