function updateCdnsStats() {

	
		
	// Bar Chart to show Current Call Stats of BISP

    var ctx = document.getElementById('barchart_blb');
   
	var data = {
		labels: ['IVR', 'Queue', 'Call Center'],
		datasets: [{
		label: 'Calls',  
		data: [calls_in_ivr, calls_in_queue, callcenter_trafic],
		backgroundColor: [
			'rgba(255, 99, 132, 1)',
			'rgba(54, 162, 235, 1)',
			'rgba(255, 206, 86, 1)'
			]
		}]
	};

	Chart.pluginService.register({
		beforeRender: function (chart) {
			if (chart.config.options.showAllTooltips) {
				// create an array of tooltips
				// we can't use the chart tooltip because there is only one tooltip per chart
				chart.pluginTooltips = [];
				chart.config.data.datasets.forEach(function (dataset, i) {
					chart.getDatasetMeta(i).data.forEach(function (sector, j) {
						chart.pluginTooltips.push(new Chart.Tooltip({
							_chart: chart.chart,
							_chartInstance: chart,
							_data: chart.data,
							_options: chart.options,
							_active: [sector]
						}, chart));
					});
				});

				// turn off normal tooltips
				chart.options.tooltips.enabled = false;
			}
		},
		afterDraw: function (chart, easing) {
			if (chart.config.options.showAllTooltips) {
				// we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
				if (!chart.allTooltipsOnce) {
					if (easing !== 1)
						return;
					chart.allTooltipsOnce = true;
				}

				// turn on tooltips
				chart.options.tooltips.enabled = true;
				Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
					tooltip.initialize();
					tooltip.update();
					// we don't actually need this since we are not animating tooltips
					tooltip.pivot();
					tooltip.transition(easing).draw();
				});
				chart.options.tooltips.enabled = false;
			}
		}
	})
	var myPieChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: {
            legend: {
        	    display: false
            },
            showAllTooltips: true,
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: "black",
                        fontSize: 10,
                        stepSize: 5,
                        beginAtZero: true
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontColor: "black",
                        fontSize: 14,
                        stepSize: 1,
                        beginAtZero: false
                    }
                }]
            }
        }
	});
    
	
	
	// End of Bar Chart to show Current Call Stats of BLB

//})

}