<!DOCTYPE HTML>
<html>

<head>

<title>Wall Board</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/w3.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<!--css-->
<link rel="stylesheet" href="css/wallboard.css">
<link rel="stylesheet" href="css/Chart.css">
<!---->

<!--scripts-->
<script type="text/javascript" src="js/loader.js"></script>
<script src="js/Chart.bundle.min.js"></script>
<script src="js/constants.js"></script>
<script src="js/weather.js"></script>
<script src="js/cdns_stats.js"></script>
<!--scripts-->

</head>

<body>

	<div class="container-fluid" style="padding: 25px;">
		<div id="cdns" class="tab1">
			<div class="row">
				<div class="col-lg-12 text-center box-skill">
					<span>Central Directorate of National Savings (CDNS)</span>

				</div>
			</div>
			<div class="row">
				<div class="col-lg-2 text-center box-size-login">
					<h1 id="total_login_agents">0</h1>
					<span>Total Login Agents </span><br><br>
				</div>
				<div class="col-lg-3 text-center box-size-idle">
					<h1 id="khi_idle">0</h1>
					<span>Ready Agents</span>
				</div>
				<div class="col-lg-3 text-center box-size-busy">
					<h1 id="khi_busy">0</h1>
					<span>Busy Agents</span>
				</div>
				<div class="col-lg-3 text-center box-size-notr">
					<h1 id="khi_notr">0</h1>
					<span>Not Ready Agents</span>
				</div>
				

				<div class="col-lg-1 text-center weather-isb">
					<img id="tkb_img" style="height: 40px; width: 40px;" class="img"
						src="https://vortex.accuweather.com/adc2010/images/slate/icons/1.svg"
						alt="Weather">
					<p id="tkb_sky"></p>
					<h3 class="temperature" id="tkb_temp"></h3>
					<div class="hi" id="tkb_hi"></div>
					<div class="low" id="tkb_low"></div>

				</div>
		

			</div>
			<div class="row calls">
				<div class="text-center box-queue">
					<h1 id="blb_queue">0</h1>
					<br> <span>Calls in Queue </span>
				</div>
				<div class="text-center box-wait">
					<h1 id="blb_waiting">00:00:00</h1>
					<br> <span>Waiting Duration </span>
				</div>
				<div class="text-center box-landed">
					<h1 id="blb_landed">0</h1>
					<br> <span>Landed Calls </span>
				</div>
				<div class="text-center box-answerd">
					<h1 id="blb_answered">0</h1>
					<br> <span>Answered Calls </span>
				</div>
				<div class="text-center box-threshold">
					<h1 id="blb_threshold">0</h1><br>
					<span>Abd. Calls Aft. Threshold</span><br>
				</div>
				<div class="text-center box-service">
					<h1 id="blb_service_lvl">0</h1>
					<br> <span>Service Level </span>
				</div>
				<div class="text-center box-abandoned">
					<h1 id="blb_abandoned">0</h1>
					<br> <span>Abandoned Calls </span>
				</div>

			</div>

			<div class="row">
				<div class="col-lg-12 text-center box-slides"></div>
			</div>
			<div class="slideshow">
				<div class="row">
					<div class="col-lg-4 text-center w-boxTable">
						<p>Top 5 Agents</p>
						<div class="row" style="margin: 0px;">
							<table class="col-lg-12 table table-responsive">
								<thead>
									<tr>
										<th>No.</th>
										<th>AgentID</th>
										<th>Total Calls</th>
										<th>Service time</th>
										<th>Avg. Handle Time</th>
									</tr>
								</thead>
								<tbody id="agentInfoRowsBLB">

								</tbody>
							</table>
						</div>
					</div>
					<div class="col-lg-4 text-center w-box">
						<p>Current Traffic</p>
						<canvas id="barchart_blb" width="200" height="80"></canvas>
					</div>
					<div class="col-lg-4 text-center w-box">
						<p>Notifications</p>
						<div class="row" style="margin: 0px; height: 160px;"
							id="notificationsBLB"></div>
					</div>
				</div>
			</div>
		</div>
		
	</div>

</body>
<script src="js/w3.js"></script>
<script src="js/jquery.min.js"></script>
<script>
	!function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (!d.getElementById(id)) {
			js = d.createElement(s);
			js.id = id;
			js.src = 'https://weatherwidget.io/js/widget.min.js';
			fjs.parentNode.insertBefore(js, fjs);
		}
	}(document, 'script', 'weatherwidget-io-js');

	!function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (!d.getElementById(id)) {
			js = d.createElement(s);
			js.id = id;
			js.src = 'https://weatherwidget.io/js/widget.min.js';
			fjs.parentNode.insertBefore(js, fjs);
		}
	}(document, 'script', 'weatherwidget-io-js');
</script>

<script>
//	myShow = w3.slideshow(".slideshow", 6000);
</script>


<script>
	$(document).ready(init);

// 	setInterval(init, 5000);
var getTop5AgentsTimer = null;
var getNotificationsTimer = null;
var getStatsForCdnsTimer = null;

function init() {
			
		 getTop5AgentsTimer = setTimeout(getTop5Agents, 2000);
		 getNotificationsTimer = setTimeout(getNotifications, 2000);
   	     getStatsForCdnsTimer = setTimeout(getStatsForCdns, 2000);

	};

	function getStatsForCdns() {
		console.log("Getting Stats of CDNS agents");

		$
				.ajax({
					url : "/wallboard/getStatsForCDNS",
					dataType : "json",
					success : function(result) {
                     clearTimeout(getStatsForCdnsTimer);
                        
						if (isEmpty(result)) {
							console.log("Empty response from server");
							getStatsForCdnsTimer = setTimeout(getStatsForCdns, 5000);
							return;
						}
						
						agents_login = result.agents_login;
						agents_idle = result.agents_idle;
						agents_busy = result.agents_busy;
						agents_notr = result.agents_notr;

						calls_in_queue = result.calls_in_queue;
						callcenter_trafic = result.callcenter_trafic ;
						calls_in_ivr = result.calls_in_ivr;
						
						if(result.calls_landed !== null){
						calls_landed = result.calls_landed;}
						if(result.calls_answered !== null){
						calls_answered = result.calls_answered;}
						if(result.calls_abandoned !== null){
						calls_abandoned = result.calls_abandoned;}
						
						
						waiting_duration=result.waiting_duration;
						if(result.call_threshold !== null){
						threshold=result.threshold;}
						if(result.service_lvl!==0){
						service_lvl=result.service_lvl;}
					
						//Agents Stats
						$('#total_login_agents').html(agents_login);
						$('#khi_idle').html(agents_idle);
						$('#khi_busy').html(agents_busy);
						$('#khi_notr').html(agents_notr);
						//Calls Stats
						$('#blb_queue').html(calls_in_queue);
						$('#blb_landed').html(calls_landed);
						$('#blb_answered').html(calls_answered);
						$('#blb_abandoned').html(calls_abandoned);
						$('#blb_waiting').html(waiting_duration);
						$('#blb_threshold').html(threshold);
						$('#blb_service_lvl').html(service_lvl);

						updateCdnsStats();
						
						getStatsForCdnsTimer = setTimeout(getStatsForCdns, 5000);
					},
					error: function (xhr, status, error) {
                        clearTimeout(getStatsForCdnsTimer);
                        getStatsForCdnsTimer = setTimeout(getStatsForCdns, 5000);
                    }
				});
	};

	
	function getTop5Agents() {
		console.log("Getting top 5 agents CDNS");
		$
				.ajax({
					url : "/wallboard/getTopAgentsCDNS",
					dataType : "json",
					success : function(result) {
                        clearTimeout(getTop5AgentsTimer);

						console.log("Get Top Agents Result " + result);
						if ((isEmpty(result))|| result.data.length==0) {
							console.log("result is empty");
	                        getTop5AgentsTimer = setTimeout(getTop5Agents, 5000);

							return;
						}
						var sNo=0;
							$("#agentInfoRowsBLB").empty();

						for (var i = 0; i < result.data.length; i++) {
							sNo++;
							var agent = result.data[i];
							$("#agentInfoRowsBLB").append(
									'<tr><td>'+ sNo +'</td><td>' + agent.agentId + '</td><td>'
											+ agent.totalCalls + '</td><td>'
											+ agent.serviceTime + '</td><td>'
											+ agent.averageHandlingTime
											+ '</td></tr>');
						}
					
						getTop5AgentsTimer = setTimeout(getTop5Agents, 5000);
					},
					error: function (xhr, status, error) {
                        clearTimeout(getTop5AgentsTimer);
                        getTop5AgentsTimer = setTimeout(getTop5Agents, 5000);
                    }
				});
	};
	
	function getNotifications() {
		console.log("Getting Notifications");
		var skill_name="CDNS";
		$
				.ajax({
					url : "/wallboard/getNotifications/" + skill_name,
					dataType : "json",
					success : function(result) {
                        clearTimeout(getNotificationsTimer);

						if (isEmpty(result)) {
							console.log("result is emmpty");
	                        getNotificationsTimer = setTimeout(getNotifications, 5000);

							return;
						}
						$("#notificationsBLB").empty();

						for (var i = 0; i < result.data.length; i++) {
							var notification = result.data[i];
							$("#notificationsBLB")
									.append(
											'<div class="col-lg-12 text-center" id="notification-div-blb"><div class="card w3-green"><h3 class="agent-state-text" style="margin-top: 6px; font-size: 15px;">'
													+ notification.notificationText
													+ '</h3></div></div>');

						}
					
						getNotificationsTimer = setTimeout(getNotifications, 5000);
					},
					error: function (xhr, status, error) {
                        clearTimeout(getNotificationsTimer);
                        getNotificationsTimer = setTimeout(getNotifications, 5000);
                    }
				});
	};

	
	function isEmpty(obj) {
	    for(var key in obj) {
	        if(obj.hasOwnProperty(key))
	            return false;
	    }
	    return true;
	}
	
	
    var time = new Date().getTime();
    $(document.body).bind("mousemove keypress", function(e) {
        time = new Date().getTime();
    });

    function refresh() {
        if(new Date().getTime() - time >= 1200000) {
        	//if there is inactivity from last 20 mins, then refresh
        	 clearTimeout(getStatsForCdnsTimer);
             clearTimeout(getNotificationsTimer);
             clearTimeout(getTop5AgentsTimer);
        	window.location.reload(true);
        }
        else 
            setTimeout(refresh, 10000);
    }

    setTimeout(refresh, 10000);

</script>


</html>